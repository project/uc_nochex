<?php
/*
 * @file
 * Process Nochex APC callback
 *
 * @param
 * None
 *
 */
  function uc_nochex_apc() {
  if (!isset($_POST['order_id'])) {
    watchdog('uc_nochex', 'APC attempted with invalid order ID', array(), WATCHDOG_ERROR);
    return;
  }

  $order_id=check_plain($_POST['order_id']);
  watchdog('uc_nochex', 'APC for order @order_id, <pre>@debug</pre>',
  array('@order_id' => $order_id, '@debug' => variable_get('uc_nochex_debug_apc', FALSE) ? print_r($_POST, TRUE) : ''));

  $order = uc_order_load($order_id);
  if ($order == FALSE) {
    watchdog('uc_nochex', 'APC attempted for non-existent order', array(), WATCHDOG_ERROR);
    return;
  }

  $apc_response=_uc_nochex_apc_confirmation();

  $duplicate = db_result(db_query("SELECT 1 FROM {uc_nochex_apc} WHERE transaction_id = %d ", $_POST['transaction_id']));
  if ($duplicate > 0) {
    if ($order->payment_method != 'credit') {
      watchdog('uc_nochex', 'APC transaction ID %id has already been processed', array('%id' => $_POST['transaction_id']), WATCHDOG_NOTICE);
    }
    return;
  }

  db_query("INSERT INTO {uc_nochex_apc} (order_id, transaction_id,  amount, apc_status,apc_response, from_email, to_email, transaction_date) VALUES (%d, %d, %f, '%s', '%s', '%s', '%s', str_to_date('%s','%%d/%%m/%%Y %%H:%%i:%%s'))",
      $_POST['order_id'], $_POST['transaction_id'], $_POST['amount'], $_POST['status'], $apc_response, $_POST['to_email'], $_POST['from_email'], $_POST['transaction_date']);

  switch ($apc_response) {
    case 'Authorised':
      $amount = $_POST['amount'];
      $transaction_id = $_POST['transaction_id'];
      $comment = t('Nochex transaction ID: @transaction_id', array('@transaction_id' => $transaction_id));
      uc_payment_enter($order_id, 'nochex', $amount, 1, NULL, $comment);
      uc_order_comment_save($order_id, 'admin', t('@amount submitted through Nochex with transaction ID @transaction', array('@amount' => uc_currency_format($amount), '@transaction' => $transaction_id)), 'order', 'payment_received', TRUE);
      break;

    case 'Declined':
      uc_order_comment_save($order_id, 1, t("Nochex declined the payment."), 'admin');
      break;


    case 'Invalid':
      watchdog('uc_nochex', 'APN transaction failed verification.', array(), WATCHDOG_ERROR);
      uc_order_comment_save($order_id, 1, t('A Nochex APN transaction failed verification for this order.'), 'admin');
  }
}

/*
 * NOCHEX APC success callback
 *
 * @param
 * $order_id
 * The order id
 * $test
 * True if test transaction, false if live transaction.
 *
 */
function uc_nochex_success($order_id=0, $test='true') {
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }
  $_SESSION['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}


/*
 * NOCHEX APC confirmation
 *
 * @param
 * None
 *
 * @return
 * string: Authorised, Declined, Invalid
 */
function uc_nochex_cancel() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Your Nochex payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));

  drupal_goto('cart');
}

/*
 * NOCHEX APC confirmation
 *
 * @param
 * None
 *
 * @return
 * string: Authorised, Declined, Invalid
 */
function _uc_nochex_apc_confirmation() {
  if (!isset($_POST)) $_POST = &$HTTP_POST_VARS;
  foreach ($_POST AS $key => $value) {
    $values[] = $key .'='. urlencode($value);
  }
  $post_fields = @implode('&', $values);

  $url = variable_get('uc_nochex_apc_url', 'https://www.nochex.com/nochex.dll/apc/apc');

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDSIZE, 0);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
  curl_setopt($ch, CURLOPT_SSLVERSION, 3);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $output = curl_exec($ch);
  curl_close($ch);

  $response = preg_replace("'Content-type: text/plain'si", "", $output);

  if ($response==="AUTHORISED") {
    return 'Authorised';
  }
  else if ($response==="DECLINED") {
    return 'Declined';
  }
  else {
    return 'Invalid';
  }
}