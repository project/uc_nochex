<?php

/**
 * @file
 * Integrates Nochex payment services and Automatic Payment Confirmation
 * (APC) with Ubercart.
 *
 * For details about Nochex visit http://www.nochex.com
 *
 * For further information on APC visit http://esupport.nochex.com/index.php?_m=knowledgebase&_a=viewarticle&kbarticleid=32
 */

/*
 * Hook Functions (Drupal)
 */

/**
 * Implementation of hook_menu().
 */
function uc_nochex_menu() {
  $items['uc_nochex/apc'] = array(
    'title' => 'Ubercart Nochex APC callback',
    'page callback' => 'uc_nochex_apc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_nochex_apc.inc',
  );
  $items['uc_nochex/success'] = array(
    'title' => 'Ubercart Nochex success callback',
    'page callback' => 'uc_nochex_success',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_nochex_apc.inc',      
  );
  $items['uc_nochex/cancel'] = array(
    'title' => 'Ubercart Nochex cancel callback',
    'page callback' => 'uc_nochex_cancel',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_nochex_apc.inc',      
  );
  return $items;
}



/**
 * Implementation of hook_form_alter().
 */
function uc_nochex_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'nochex') {
      $form_state['uc_nochex']['order'] = $order;
      _uc_nochex_add_form_elements($form, $order);  
    }
  }
}

/**
 * Implementation of hook_help().
 */
function uc_nochex_help($path, $arg) {
  if ($path != 'admin/help#uc_nochex') {
    return;
  }
  $link = 'http://www.nochex.com';  

  $text = t('The Nochex payment module provides Ubercart with a <a href="@nochex_url">Nochex</a> payment facility with Automatic Payment Confirmation. ', array('@nochex_url' => $link));
  $text .= t('<a href="@nochex_url">Nochex</a> is a UK based online payment company, specialising in providing smaller online businesses with simple, accessible, easy to use, online payment services.', array('@nochex_url' => $link));    
  return '<p>'. $text .'</p>';
}

/*
 * Hook Functions (Ubercart)
 */

/**
 * Implementation of hook_payment_method().
 */
function uc_nochex_payment_method() {
  $title = variable_get('uc_nochex_method_title', t('Nochex - secure payment by credit/debit card'));
  $methods[] = array(
    'id' => 'nochex',
    'name' => t('Nochex'),
    'title' => $title .'<br /><img src="https://www.nochex.com/logobase-secure-images/non-secure-nomess-line-sma.gif">',
    'review' => t('Nochex'),
    'desc' => t('Redirect users to submit payments through Nochex.'),
    'callback' => 'uc_payment_method_nochex_admin',
    'weight' => 3,
    'checkout' => FALSE,
    'no_gateway' => TRUE,
  );

  return $methods;
}


/**
 * Callback for Nochex payment method settings.
 */

function uc_payment_method_nochex_admin($op, &$arg1) {
  switch ($op) {
    case 'order-view':
      $result = db_query("SELECT transaction_id,amount FROM {uc_nochex_apc} WHERE order_id = %d ORDER BY transaction_date ASC", $arg1->order_id);
      $row=db_fetch_object($result);
      return t('Transaction @transaction_id: @amount', array('@transaction_id' => $row->transaction_id, '@amount' => uc_currency_format($row->amount)));   
    case 'settings':
      $form['uc_nochex_hidebilling'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide billing address on Nochex payment page, so customers cant edit it. If this value is set, the billing address passed to Nochex will be set to the delivery address.'),
        '#default_value' => variable_get('uc_nochex_hidebilling', FALSE),
      );
      $form['uc_nochex_sid'] = array(
        '#type' => 'textfield',
        '#title' => t('Nochex email address'),
        '#description' => t('The Nochex merchant id required to identify your account.'),
        '#default_value' => variable_get('uc_nochex_sid', ''),
      );
      $form['uc_nochex_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#description' => t('The text that will appear on the Upcart checkout screen against the Nochex payment method option'),
        '#default_value' => variable_get('uc_nochex_method_title', t('Secure credit/debit card payment')),
      );
      $form['uc_nochex_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Order review submit button text'),
        '#description' => t('Provide Nochex specific text for the submit button on the order review page.'),
        '#default_value' => variable_get('uc_nochex_checkout_button', t('Submit Order')),
      );      
      $form['uc_nochex_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Nochex URL'),
        '#description' => t('The URL used for posting information to Nochex.'),
        '#default_value' => variable_get('uc_nochex_url', 'https://secure.nochex.com'),
      );
      $form['uc_nochex_apc_url'] = array(
        '#type' => 'textfield',
        '#title' => t('APC confirmation URL'),
        '#description' => t('The URL to confirm receipt of information from Nochex.'),
        '#default_value' => variable_get('uc_nochex_apc_url', 'https://www.nochex.com/nochex.dll/apc/apc'),
      );
      $form['uc_nochex_debug_apc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Debug APC callback'),
        '#description' => t('Display debug information in log'),
        '#default_value' => FALSE,
      );
      $form['uc_nochex_test'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable test mode'),
        '#description' => t('process payments through Nochex without any money being taken.'),
        '#default_value' => variable_get('uc_nochex_test', FALSE),
      );
      $form['uc_nochex_test_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Site base URL for test callback'),
        '#description' => t('An externally accessable URL to your site root.'),
        '#default_value' => variable_get('uc_nochex_test_url', url('', array('absolute' => TRUE))),
      );
    return $form;
  }
}


/*
 * Add Nochex payment submission fields to form
 *
 * @param
 * $form
 * The form
 * $order
 * The order
 *
 */
function _uc_nochex_add_form_elements(&$form, $order) { 
  $test=variable_get('uc_nochex_test', TRUE);
  if ($test) {
    $test_base_url = variable_get('uc_nochex_test_url', url('', array('absolute' => TRUE))); 
    $callback_url = $test_base_url .'/uc_nochex/apc';
  }
  else {
    $callback_url  = url('uc_nochex/apc', array('absolute' => TRUE));
  }     
  watchdog('uc_nochex', 'APN callback: %callback', array('%callback' => $callback_url));   
  $data = array(
    'merchant_id' => variable_get('uc_nochex_sid', ''),
    'description' => 'HIAM on-line order',
    'amount' => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    'order_id' => $order->order_id,
    'optional_1' => uc_cart_get_id(),
    'callback_url' => $callback_url,
    'success_url'  => url('uc_nochex/success/'. $order->order_id, array('absolute' => TRUE)),    
    'cancel_url'  => url('uc_nochex/cancel', array('absolute' => TRUE)),
    'delivery_fullname' =>  drupal_substr($order->delivery_first_name .' '. $order->delivery_last_name, 0, 128),
    'delivery_address' => drupal_substr($order->delivery_street1 .' '. $order->delivery_street2 .' '. $order->delivery_city, 0, 256),
    'delivery_postcode' => drupal_substr($order->delivery_postal_code, 0, 16),
    'customer_phone_number' => drupal_substr($order->delivery_phone, 0, 16),  
    'email_address' => drupal_substr($order->primary_email, 0, 64),
    'hide_billing_details' => variable_get('uc_nochex_hidebilling', TRUE) ? 'true' : 'false',
    'test_transaction' => $test ? '100' : '',
    'test_success_url' => url('uc_nochex/success/'. $order->order_id, array('absolute' => TRUE)),    
  );
  if (variable_get('uc_nochex_hidebilling', TRUE)) {
    $data['billing_fullname'] = $data['delivery_fullname'];
    $data['billing_address'] = $data['delivery_address'];
    $data['billing_postcode'] = $data['delivery_postcode'];
  }
  else {
    $data['billing_fullname'] = drupal_substr($order->billing_first_name .' '. $order->billing_last_name, 0, 128);
    $data['billing_address'] = drupal_substr($order->billing_street1 .' '. $order->billing_street2 .' '. $order->billing_city, 0, 256);
    $data['billing_postcode'] = drupal_substr($order->billing_postal_code, 0, 16);
  }
  
  
  $form['#action'] = variable_get('uc_nochex_url', 'https://secure.nochex.com');

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => variable_get('uc_nochex_checkout_button', t('Submit Order')),
  );
    
}